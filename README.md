## Inhaltsverzeichnis

1. [Allgemeine Informationen](#general-info)
2. [Technologien](#technologien)
3. [Schriften](#schriften)
4. [Icons](#icons)

### Allgemeine Informationen

---

Abschlussprüfung 2021/22 Prüflingsnummer: 65759

### Screenshot

![Image text](images/start/kloster-readme.png)

---

## Technologien

Eine Liste der im Rahmen des Projekts verwendeten Technologien:

-   [Visual Studio Code](https://www.w3schools.com/bootstrap/bootstrap_ref_js_scrollspy.asp): Visual Studio Code ist ein optimierter Code-Editor mit Unterstützung für Entwicklungsvorgänge wie Debugging, Task-Ausführung und Versionskontrolle. Es zielt darauf ab, nur die Werkzeuge bereitzustellen, die ein Entwickler für einen schnellen Code-Build-Debug-Zyklus benötigt, und überlässt komplexere Arbeitsabläufe voll ausgestatteten IDEs wie Visual Studio IDE.

-   [JS Scrollspy](https://www.w3schools.com/bootstrap/bootstrap_ref_js_scrollspy.asp): Das Scrollspy-Plugin dient zur automatischen Aktualisierung von Links in einer Navigationsliste auf der Grundlage der Scrollposition.

-   [affix.js](https://www.w3schools.com/bootstrap/bootstrap_ref_js_affix.asp): Das Affix-Plugin ermöglicht es, ein Element an einem Bereich auf der Seite zu befestigen (zu fixieren). Das Plugin schaltet dieses Verhalten ein und aus (ändert den Wert der CSS-Position von statisch auf fest), abhängig von der Bildlaufposition.

-   [Bootstrap 5.1](https://getbootstrap.com/docs/5.0/getting-started/introduction/): Open-Source-Toolkit für Frontends, mit Sass-Variablen und -Mixins, responsivem Grid-System, umfangreichen vorgefertigten Komponenten und leistungsstarken JavaScript-Plugins

-   [Magnific Popup](https://dimsemenov.com/plugins/magnific-popup/): Magnific Popup ist ein reaktionsfähiges Leuchtkasten & Dialog-Skript mit Fokus auf Leistung und Bereitstellung der besten Erfahrung für Benutzer mit jedem Gerät

-   [AOS.js](https://github.com/michalsnik/aos): Animate On Scroll Bibliothek. Standardmäßig achtet AOS auf DOM-Änderungen und ruft automatisch refreshHard auf, wenn neue Elemente asynchron geladen werden oder wenn etwas aus dem DOM entfernt wird. In Browsern, die MutationObserver nicht unterstützen, wie IE, müssen Sie AOS.refreshHard() möglicherweise selbst aufrufen.

-   [accessibilityButtons.js](https://tiagoporto.github.io/accessibility-buttons/): Schaltflächen zum Hinzufügen/Entfernen des Kontrasts und zum Erhöhen/Verringern der Schriftgröße.

---

## Schriften

Diese Website verwendet 2 Arten von Schriftarten

-   [Butlers](https://www.behance.net/gallery/27753367/Butler-FREE-FONT-14-weights)

-   [Jost](https://fonts.google.com/specimen/Jost)

Kostenlos für private und kommerzielle Nutzung!

---

## Icons

Diese Website verwendet eigene Icons und Fontawesome Icons

-   [fontawesome](https://fontawesome.com/)

Kostenlos für private und kommerzielle Nutzung!

---
